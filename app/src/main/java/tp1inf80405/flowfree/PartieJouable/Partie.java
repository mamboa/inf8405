package tp1inf80405.flowfree.PartieJouable;

/**
 * Cette classe définit une partie proprement dite. C'est elle qui
 * permet de savoir si une partie est gagnée, perdue ou inachevée
 * Created by TP1 on 1/02/2016.
 */
public class Partie {

    private Jeu jeu_;
    private int nombreDeCasesOccupees_;

    public Partie(Jeu jeu) {
        jeu_ = jeu;
        nombreDeCasesOccupees_ = 0;
    }

    public boolean siTermine() {
        int size = jeu_.obtenirTaille();
        return nombreDeCasesOccupees_ == size * size;
    }

    public void assignerCasesOccupees(int num) {
        nombreDeCasesOccupees_ = num;
    }

    public Jeu obtenirJeu() {
        return jeu_;
    }

    public void reinitialiser() { nombreDeCasesOccupees_ = 0;  }
}
