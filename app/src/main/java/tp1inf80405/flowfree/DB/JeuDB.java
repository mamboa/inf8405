package tp1inf80405.flowfree.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by TP1 on 07/02/2016.
 */
public class JeuDB {

    SQLiteDatabase db_;
    DbHelper dbHelper_;
    Context context_;

    public JeuDB(Context context) {
        context_ = context;
    }

    /**
     * obtenir un/e jeu/partie dont l'identifiant entier est donne en entre
     * @param id
     * @return
     */
    public Cursor get(int id) {
        //ouverture en lecture
        dbHelper_ = new DbHelper(context_);
        db_ = dbHelper_.getReadableDatabase();

        String[] cols = DbHelper.TablePartiesCols;
        Cursor cursor = db_.query( DbHelper.TableParties,
                cols, cols[0] + "=" + id, null, null, null, null);
        return cursor;
    }

    /**
     * obtenir toutes les parties/jeux
     * @return le curseur résultat de la requete
     */
    public Cursor getAll() {
        //ouverture en lecture
        dbHelper_ = new DbHelper(context_);
        db_ = dbHelper_.getReadableDatabase();

        Cursor cursor = db_.query( DbHelper.TableParties,
                DbHelper.TablePartiesCols, null, null, null, null, null);
        return cursor;
    }

    public long insert(int id, boolean termine) {
        String[] cols = DbHelper.TablePartiesCols;
        ContentValues contentValues = new ContentValues();
        contentValues.put(cols[0], id);
        contentValues.put(cols[1], termine ? "1":"0");
        //ouverture en ecriture
        dbHelper_ = new DbHelper(context_);
        db_ = dbHelper_.getWritableDatabase();

        long value = db_.insert(DbHelper.TableParties, null, contentValues);
        close();
        return value;
    }

    public long update(int id,boolean termine) {
        String[] cols = DbHelper.TablePartiesCols;
        ContentValues contentValues = new ContentValues();
        contentValues.put( cols[1], termine ? "1" : "0" );
        //ouverture en ecriture
        dbHelper_ = new DbHelper(context_);
        db_ = dbHelper_.getWritableDatabase();

        long temoin = db_.update(DbHelper.TableParties,
                contentValues,
                cols[0] + "=" + id, null );
        close();
        return temoin;
    }


    public void close() {
        db_.close();
    }
}