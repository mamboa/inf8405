package tp1inf80405.flowfree.Terrain;

/**
 * Point
 * Cette classe définit un point représenté par un cercle au milieu d'une case
 * Created by TP1 on 28/01/2016.
 */
public class Point {

    //case dans laquelle se trouve le point
    private Case case_;
    //indice de la couleur du point
    private int indiceCouleur_;

    /**
     * Constructeur
     * @param uneCase
     * @param indiceCouleur
     */
    public Point(Case uneCase, int indiceCouleur) {
        case_ = uneCase;
        indiceCouleur_ = indiceCouleur;
    }

    /**
     * Constructeur
     * @param x
     * @param y
     * @param indiceCouleur
     */
    public Point(int x, int y, int indiceCouleur) {
        case_ = new Case(x, y);
        indiceCouleur_ = indiceCouleur;
    }

    //accesseurs
    public Case obtenirCase() {
        return case_;
    }
    public int obtenirIndiceCouleur() {
        return indiceCouleur_;
    }
}
