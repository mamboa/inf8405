package tp1inf80405.flowfree.Activities.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import tp1inf80405.flowfree.Activities.Fragments.Fragment7;
import tp1inf80405.flowfree.Activities.Fragments.Fragment8;

public class TabPagerAdapter extends FragmentStatePagerAdapter {


    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new Fragment7();
            case 1:
                return new Fragment8();
        }
        return null;
    }
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return 2; //Number of Tabs
    }
}
