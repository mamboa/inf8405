package tp1inf80405.flowfree.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import tp1inf80405.flowfree.DB.JeuDB;
import tp1inf80405.flowfree.PartieJouable.Jeu;
import tp1inf80405.flowfree.PartieJouable.SingletonJeu;
import tp1inf80405.flowfree.R;
import tp1inf80405.flowfree.Terrain.Point;

/**
 * Created by TP1 on 02/02/2016.
 */
public class MenuActivity extends ActionBarActivity {

    Button grille7Button;
    Button grille8Button;
    private SingletonJeu singletonJeu_ = SingletonJeu.getInstance();
    private JeuDB jeuDB_ = new JeuDB(this);
    private Cursor cursor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuactivity);

        try {
            List<Jeu> jeux = new ArrayList<Jeu>();
            chargerLesCartes(getAssets().open("cartes/cartes.xml"), jeux);
            singletonJeu_.jeux_ = jeux;
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
        grille7Button = (Button) findViewById(R.id.grille7x7);
        grille7Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GridPickerActivity.class);
                intent.putExtra("choix", 0);
                startActivity(intent);
            }
        });

        grille8Button = (Button) findViewById(R.id.grille8x8);
        grille8Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), GridPickerActivity.class);
                intent.putExtra("choix",1);
                startActivity(intent);
            }
        });


    }
    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * quitter le jeu
     */
    @Override
    public void onBackPressed(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setTitle("Quitter?")
                .setMessage("Voulez-vous vraiment quitter ce jeu?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing

                    }
                })
               ;
        dialog.show();
    }

    private void chargerLesCartes(InputStream inputStream, List<Jeu> jeux) {
        try {
            cursor = jeuDB_.getAll();
            int counter = cursor.getCount();

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputStream);
            int taille7 = singletonJeu_.taille7;
            int taille8 = singletonJeu_.taille8;

            NodeList nList = doc.getElementsByTagName( "partie" );
            for (int indicePartie = 0; indicePartie < nList.getLength(); indicePartie++){
                Node nNode = nList.item(indicePartie);
                if ( nNode.getNodeType() == Node.ELEMENT_NODE ){
                    Element eNode = (Element) nNode;

                    String taille = eNode.getElementsByTagName("taille").item(0).getFirstChild().getNodeValue();
                    if(Integer.parseInt(taille)== 7)
                        taille7++;
                    if(Integer.parseInt(taille)== 7)
                        taille8++;

                    String lesPoints = eNode.getElementsByTagName("points").item(0).getFirstChild().getNodeValue();
                    String lesChiffres = lesPoints.replaceAll("[, ; ()]", "");

                    ArrayList<Point> points = new ArrayList<Point>();
                    for(int i = 0; i < lesChiffres.length(); i += 5){
                        Point premierPoint = new Point(Character.getNumericValue(lesChiffres.charAt(i)),        // x
                                Character.getNumericValue(lesChiffres.charAt(i + 1)),                           // y
                                Character.getNumericValue(lesChiffres.charAt(i + 4))-1);                        // couleur
                        Point deuxiemePoint = new Point(Character.getNumericValue(lesChiffres.charAt(i + 2)),   // x
                                Character.getNumericValue(lesChiffres.charAt(i + 3)),                           // y
                                Character.getNumericValue(lesChiffres.charAt(i + 4))-1);                        //couleur
                        points.add(premierPoint);
                        points.add(deuxiemePoint);
                    }

                    if(counter == 0){
                        jeuDB_.insert(indicePartie, false);
                        jeuDB_.close();
                    }
                    jeux.add(new Jeu(indicePartie, Integer.parseInt(taille), points));
                }
            }
            singletonJeu_.taille7 = taille7;
            singletonJeu_.taille8 = taille8;
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }
}

